/*********************************************************************
* Nombre del fichero:   crackerMPI.c
* Autor:     Julen San Tirso
*********************************************************************/

/**************************** LIBRERIAS  ****************************/
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <unistd.h>
#if defined(__APPLE__)
#  define COMMON_DIGEST_FOR_OPENSSL
#  include <CommonCrypto/CommonDigest.h>
#  define SHA1 CC_SHA1
#else
#  include <openssl/sha.h>
#endif
#define NUM 1000

/***********************DEFINICION DE FUNCIONES *********************/

char inicioASCII = 'a';
char finASCII = 'z';
int min = 1;
int max = 3;
char *password;
char *c;
int actualSize;
char combinaciones[NUM][15];

int inc(char *c) {
	if(c[0]==0){
		return 0;
	}
	if(c[0]==finASCII){
		c[0]=inicioASCII;
		return inc(c+sizeof(char));
	}   
	c[0]++;
	return 1;
}

char *str2sha1(const char *str, int length) {
    int n;
    SHA_CTX c;
    unsigned char digest[20];
    char *out = (char*)malloc(41);

    SHA1_Init(&c);

    while (length > 0) {
        if (length > 512) {
            SHA1_Update(&c, str, 512);
        } else {
            SHA1_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    SHA1_Final(digest, &c);

    for (n = 0; n < 20; ++n) {
        snprintf(&(out[n*2]), 20*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

void inputVariables(int argc, char* argv[]){
	int c;
	while ((c = getopt (argc, argv, "m:M:c:C:p:P:")) != -1)
         switch (c)
           {
           case 'm':
             sscanf(optarg, "%d", &min);
             if(min<1){
             	min=1;
             }
             break;
           case 'M':
             sscanf(optarg, "%d", &max);
             if(max<2){
             	max=2;
             }
             break;
           case 'c':
             sscanf(optarg, "%c", &inicioASCII);
             break;
           case 'C':
             sscanf(optarg, "%c", &finASCII);
             break;
           case 'P':
           case 'p':
             password = (char*)malloc(41);
             password = optarg;
             break;
           default:
           abort ();
           }
}

void generarCombinaciones() {
	int i, j;
	int cont = -1;
	
	for(i=min;i<=max&&cont<NUM;i++) {
		for(j=0;j<i;j++) c[j]=inicioASCII;
		c[i]=0;
		do {
			cont++;
			strcpy(combinaciones[cont], c);
		
			if (cont == NUM-1) {
				cont=NUM;
			}
        } while(inc(c)&&cont<NUM);
		
		if (cont<NUM) {
			for(j=0;j<i+1;j++) c[j]=inicioASCII;
			c[i+1]=0;
		}
	}
}

void generarSiguientes() {
	int i, j;
	int cont = -1;
	
	if (strlen(c)>max) {
		printf("Se ha llegado al limite de caracteres y no se ha encontrado la contraseña.\n");
		exit(0);
	}
	
	for(i=strlen(c);i<=max&&cont<NUM;i++) {
		do {
			cont++;
			strcpy(combinaciones[cont], c);
			
			if (cont == NUM-1) {
				cont=NUM;
			}
		} while(inc(c)&&cont<NUM);
		
		if (cont<NUM) {
			for(j=0;j<i+1;j++) c[j]=inicioASCII;
			c[i+1]=0;
		}
	}
}

int main(int argc, char **argv) {
	inputVariables(argc, argv);
	
	if (min > max) {
		int temp = min;
		min = max;
		max = temp;
	}
	
	int rank, size;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	int subsize;
	subsize = NUM / size;
	char subset[subsize][15];

	if (rank==0) {
		printf("El minimo es de %d \n", min);
		printf("El maximo es de %d \n", max);
		printf("El caracter inicial es %c \n", inicioASCII);
		printf("El caracter final es %c \n\n\n", finASCII);
		c = malloc((max+1)*sizeof(char));
		generarCombinaciones();
	}

	int g;
	int result;
	char *output2;

	MPI_Scatter(&combinaciones, sizeof(char)*15*subsize, MPI_CHAR, &subset, sizeof(char)*15*subsize, MPI_CHAR, 0, MPI_COMM_WORLD);  
	for (g=0; g<subsize; g++) {
		output2 = str2sha1(subset[g], strlen(subset[g]));
		result = strncmp(password, output2, 40);
		
		if(result==0){
			printf("Encontrado! La password es: %s\n",subset[g]);
			exit(0);
		}
	}
	
	if (rank==0) {
		int resto1, m1;
		resto1 = size * subsize;
		for (m1=resto1; m1<NUM; m1++) {
			output2 = str2sha1(combinaciones[m1], strlen(combinaciones[m1]));
			result = strncmp(password, output2, 40);
			if(result==0){
				printf("Encontrado! La password es: %s\n",combinaciones[m1]);
				exit(0);
			}
		}
	}

	while (1) {
		if (rank==0) {
			generarSiguientes();
		}
		
		MPI_Scatter(&combinaciones, sizeof(char)*15*subsize, MPI_CHAR, &subset, sizeof(char)*15*subsize, MPI_CHAR, 0, MPI_COMM_WORLD);
		for (g=0; g<subsize; g++) {
			output2 = str2sha1(subset[g], strlen(subset[g]));
			result = strncmp(password, output2, 40);
			
			if(result==0){
				printf("Encontrado! La password es: %s\n",subset[g]);
				exit(0);
			}
		}
		if (rank==0) {
			int resto, m;
			resto = size * subsize;
			for (m=resto; m<NUM; m++) {
				output2 = str2sha1(combinaciones[m], strlen(combinaciones[m]));
				result = strncmp(password, output2, 40);
				if(result==0){
					printf("Encontrado! La password es: %s\n",combinaciones[m]);
					exit(0);
				}
			}
		}
	}
	
	free(c);
	MPI_Finalize();
	
	return 0;
}

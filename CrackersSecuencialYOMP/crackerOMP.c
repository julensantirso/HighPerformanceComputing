/*********************************************************************
* Nombre del fichero:   crackerOMP.c
* Autor:     Julen San Tirso
*********************************************************************/

/**************************** LIBRERIAS  ****************************/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <openssl/sha.h>

/***********************DEFINICION DE FUNCIONES *********************/
char inicioASCII = 'a';
char finASCII = 'z';
int min = 1;
int max = 3;
int numhilos=1;
char *password;
char *characters;

void Hello() {
	int my_rank = omp_get_thread_num();
	int thread_count = omp_get_num_threads();
	printf("Hello from thread %d of %d\n", my_rank, thread_count);
}

int inc(char *c){
    if(c[0]==0){
		return 0;
	}
    if(c[0]==finASCII){
    	c[0]=inicioASCII;
        return inc(c+sizeof(char));
    }   
    c[0]++;
    return 1;
}

char *str2sha1(const char *str, int length) {
    int n;
    SHA_CTX c;
    unsigned char digest[20];
    char *out = (char*)malloc(41);

    SHA1_Init(&c);

    while (length > 0) {
        if (length > 512) {
            SHA1_Update(&c, str, 512);
        } else {
            SHA1_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    SHA1_Final(digest, &c);

    for (n = 0; n < 20; ++n) {
        snprintf(&(out[n*2]), 20*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

void probar() {
	
	int my_rank = omp_get_thread_num();
	int thread_count = omp_get_num_threads();
	Hello();
	int miNumero = my_rank;
	
	while(miNumero<=(finASCII-inicioASCII)) {
		char miLetra = characters[miNumero];
		int i;
		int j;
		char *c = malloc((max+2)*sizeof(char));
		c[0] = inicioASCII;
		int result;
		char *output;
		#pragma omp parallel for
		for(i=min-1;i<=max-1;i++){
		    for(j=1;j<i;j++) c[j]=inicioASCII;
		    c[i]=0;
		    do {
		    	int size = strlen(c);
		    	char *c2 = malloc(size+2);
		    	strcpy(c2, c);
		    	c2[size+0]=miLetra;
		    	c2[size+1]=0;
			int my_rank = omp_get_thread_num();
			int thread_count = omp_get_num_threads();
			//printf("Thread %d of %d\n testing: %s\n", my_rank, thread_count, c2);
		    	output = str2sha1(c2, strlen(c2));
		    	
		    	result = strncmp(password, output, 40);
				
		        if(result==0){
		        	printf("Encontrado! %s:\t%s\n",c2,output);
				Hello();
		        	exit(0);
			}
				
			free(c2);
			
		} while(inc(c) && result!=0);
					
		}
		free(output);
		miNumero = miNumero+thread_count;
		free(c);
	}
}
void inicializarVariables() {
	int num = finASCII-inicioASCII;
	printf("numero de caracteres: %d\n",num);
	characters = (char*)malloc(num);
	
	char letter = inicioASCII;
	int i=0;
	do {
		characters[i] = letter;
		letter++;
		i++;
	} while(letter != finASCII+1);
}

void inputVariables(int argc, char* argv[]){
	int c;
	while ((c = getopt (argc, argv, "m:M:c:C:p:P:h:")) != -1)
         switch (c)
           {
		   case 'h':
		     sscanf(optarg, "%d", &numhilos);
		     if(numhilos<1){
		     	numhilos=1;
		     }
		     break;
		   case 'm':
		     sscanf(optarg, "%d", &min);
		     if(min<2){
		     	min=2;
		     }
		     break;
		   case 'M':
		     sscanf(optarg, "%d", &max);
		     if(max<2){
		     	max=2;
		     }
		     break;
		   case 'c':
		     sscanf(optarg, "%c", &inicioASCII);
		     break;
		   case 'C':
		     sscanf(optarg, "%c", &finASCII);
		     break;
		   case 'P':
		   case 'p':
		     password = (char*)malloc(41);
		     password = optarg;
		     break;
		   case '?':
		     if (optopt == 'c')
		       fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		     else if (isprint (optopt))
		       fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		     else
		       fprintf (stderr,
		                "Unknown option character `\\x%x'.\n",
		                optopt);
		   default:
		     abort ();
           }
           inicializarVariables();
           
}

int main(int argc, char* argv[]){
	inputVariables(argc, argv);
	printf("ejecutando con %d hilos\n",numhilos);
	#pragma omp parallel num_threads(numhilos)
	probar();
	free(password);
	free(characters);
	return 0;
}

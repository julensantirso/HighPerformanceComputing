instalar libreria
 sudo apt-get instal libssl-dev

 sudo apt-get instal openssl


Cracker secuencial
 	-Para compilar: gcc crackerSecuencial.c -lssl -lcrypto -o crackerSecuencial; chmod +x ./crackerSecuencial; ./ crackerSecuencial
	-Para ejecutar: ./crackerSecuencial -m 1 -M 5 -p (chorro del hash).

	En la compilacion usamos librerias, por eso metemos lssl....
	En el caso de la ejecucion los valores de m(numero minimo de letras) y M(numero maximo)

Cracker OpenMP
 	-Para compilar: gcc -fopenmp crackerOMP.c -lssl -lcrypto -o crackerOMP; 
	-Para ejecutar: ./crackerOMP -m 1 -M 5 -p (chorro del hash) -h 2.

	En la compilacion usamos librerias, por eso metemos lssl....
	En el caso de la ejecucion los valores de m(numero minimo de letras), M(numero maximo) y h(numero de hilos).

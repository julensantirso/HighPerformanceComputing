/*********************************************************************
* Nombre del fichero:   crackerSecuencial.c
* Autor:     Julen San Tirso
*********************************************************************/

/**************************** LIBRERIAS  ****************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <openssl/sha.h>

/***********************DEFINICION DE FUNCIONES *********************/

char inicioASCII = 'a';
char finASCII = 'z';
int min = 1;
int max = 3;
char *password;
char *characters;

int inc(char *c){
    if(c[0]==0) return 0;
    if(c[0]=='z'){
        c[0]='0';
        return inc(c+sizeof(char));
    }   
    c[0]++;
    return 1;
}

char *strSHA1(const char *str, int length) {
    int n;
    SHA_CTX c;
    unsigned char digest[20];
    char *out = (char*)malloc(41);

    SHA1_Init(&c);

    while (length > 0) {
        if (length > 512) {
            SHA1_Update(&c, str, 512);
        } else {
            SHA1_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    SHA1_Final(digest, &c);

    for (n = 0; n < 20; ++n) {
        snprintf(&(out[n*2]), 20*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

void inputVariables(int argc, char* argv[]){
	int c;
	while ((c = getopt (argc, argv, "m:M:c:C:p:P:")) != -1)
         switch (c)
           {
           case 'm':
             sscanf(optarg, "%d", &min);
             if(min<1){
             	min=1;
             }
             break;
           case 'M':
             sscanf(optarg, "%d", &max);
             if(max<2){
             	max=2;
             }
             break;
           case 'c':
             sscanf(optarg, "%c", &inicioASCII);
             break;
           case 'C':
             sscanf(optarg, "%c", &finASCII);
             break;
           case 'P':
           case 'p':
             password = (char*)malloc(41);
             password = optarg;
             break;
           case '?':
             if (optopt == 'c')
               fprintf (stderr, "Option -%c requires an argument.\n", optopt);
             else if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
           default:
             abort ();
           }
           
}

int main(int argc, char **argv) {
    inputVariables(argc, argv);
    
    int i,j;
    char *c = malloc((max+1)*sizeof(char));
    int result;
    char *output;
    printf("Comienza el testing: \n");
    for(i=min;i<=max;i++){
        for(j=0;j<i;j++) c[j]='0';
        c[i]=0;
        do {
        	output = strSHA1(c, strlen(c));
        	
        	result = strncmp(password, output, 40);

            if(result==0){
            	printf("Encontrado! %s:\t%s\t%s\n",c,output,password);
            	exit(0);
            }
            
            free(output);
            
        } while(inc(c) && result!=0);
    }
    free(c);
    free(password);
    return 0;
}

